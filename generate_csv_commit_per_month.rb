#!/usr/bin/ruby

require 'net/http'
require 'rexml/document'

if ARGV.length != 2
  STDERR.puts "This script will generate a CSV file with month and the quantity of commits in this month."
  STDERR.puts "usage: #{__FILE__} [PROJECT_NAME] [NUMBER_OF_ACTUAL_MONTH]"
  exit 1
end

project_name = ARGV[0]
actual_month = ARGV[1]

url = "https://www.openhub.net/p/%s/analyses/latest/commitshistory.xml" % project_name

response = Net::HTTP.get_response(URI(url))

if response.code != '200'
  STDERR.puts "#{response.code} - #{response.message}"
  exit 1
end

data = REXML::Document.new(response.body).text
commits = data.scan(/,(\d+)\],/)

history = Hash.new
month = actual_month.to_i - 1
commits = commits.reverse

for index in 0 ... commits.size
  history[month] = commits[index]
  month -= 1
end

history.each do |index, val| 
  puts "#{index} => #{val}"
end

